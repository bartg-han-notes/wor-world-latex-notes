# WoR - World - LaTeX notities

In deze repository staan mijn notities van WoR World, in LaTeX.
Deze taal heeft voor dit vak het voordeel dat naast normale notities er ook wiskundige notities relatief gemakkelijk in gemaakt kunnen worden.

## PDF genereren

Om een pdf te genereren vanaf de .tex bestanden raad ik de [LaTeX workshop extensie voor VS code](https://marketplace.visualstudio.com/items?itemName=James-Yu.latex-workshop) aan.
Met deze extensie kan je de pdf genereren vanaf het extensie menu, of met de startknop bij notes.tex.
Als de pdf is gegenereerd kan je het bestand openen in een pdf lezer, or met de geïntegreerde pdf lezer bekijken.

Mogelijk moet je nog een aantal packages installeren voor je latex bestanden kan omzetten naar een pdf. Ik heb hiervoor dit uitgevoerd:
```bash
sudo apt install texlive texlive-latex-extra texlive-extra-utils latexmk
```

## Bestandstructuur

 * **notes.tex** is het hoofdbestand van de pdf.
    Dit bestand maakt gebruik van de andere bestanden, en definieert zelf de packages die deze andere bestanden kunnen gebruiken.
 * **sections** is een folder met hier in alle .tex bestanden die een concept beschrijven wat tijdens de les is uitgelegd.
 * **images** bevat alle afbeeldingen die worden gebruikt in de secties van het document.
    De meeste afbeeldingen worden gegenereerd uit het PowerPoint bestand in de submap `src`.

## Meewerken
Als je een fout wil verbeteren, of iets wil toevoegen kan dit!
Als het goed is kan je via een pull request direct de .tex bestanden aanpassen.
